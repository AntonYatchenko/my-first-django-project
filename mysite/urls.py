from django.conf.urls import include, url
from django.contrib import admin

from mysite.views import  Page1, Page2, Page3, HomeView

urlpatterns = [
    url(r'^$', HomeView.as_view()),
    url(r'^polls/', include('polls.urls')),
    url(r'^admin/', admin.site.urls),


    url(r'^page1/', Page1.as_view()),
    url(r'^page2/', Page2.as_view()),
    url(r'^page3/', Page3.as_view()),
    ]