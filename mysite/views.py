from django.http.response import HttpResponse
from django.shortcuts import render
from django.views.generic import TemplateView

from mysite.models import Dish


class HomeView(TemplateView):
    template_name = "home.html"
    def get_context_data(self, **kwargs):
        context = {
            'dishes':Dish.objects.all()
        }
        return context



class Page1(TemplateView):
    template_name = "page1.html"
    def get_context_data(self, **kwargs):
        context = {
            'names': ['Page1', 'Anton', 'Sabina'],
        }
        return context




class Page2(TemplateView):
    template_name = "page2.html"
    def get_context_data(self, **kwargs):
        context = {
            'names': ['Page2', 'Pavel','Mekugirl'],
        }
        return context




class Page3(TemplateView):
    template_name = "page3.html"
    def get_context_data(self, **kwargs):
        context = {
            'names': ['Page3', 'Rinat','Dima'],
        }
        return context
